# README #

How to configure the human awareness functionalities

### INSTALLATION MOTION PLANNING ###

* Install [motion planner](https://bitbucket.org/iras-ind/human_aware_motion_planners) *branch master*
* Install [wrapper](https://CNR-ITIA@bitbucket.org/iras-ind/pickplace_human_wrapper.git) *branch master*
* Install [tofas package](https://bitbucket.org/iras-ind/tofas_human_awareness) *branch master*


### CONFIGURATION ###

* Modify _YOUR_PACKAGE_moveit_config/launch/trajectory_execution.launch.xml [these instructions](ur_modern_driver_3.md).
* Follow the instruction for configure the MoveIt! planner [instructions](https://bitbucket.org/iras-ind/human_aware_motion_planners/src/master/dirrt_star/readme.md)
* Modify tofas_human_awareness/config/human_detection_wrapper.yaml following the comments in the [file](tofas_human_awareness/config/human_detection_wrapper.yaml)
* Modify tofas_human_awareness/config/safety_iso15066.yaml following the comments in the [file](tofas_human_awareness/config/safety_iso15066.yaml)

### USAGE ###

* launch MoveIt! and ur_modern_driver using your applications launchers.
* launch: _roslaunch tofas_human_awareness tofas_human_awareness.launch_ to execute the wrapper between the human_detection, the off-line motion planner.


### INSTALLATION CONTROLLER ###

* execute the script installation_script_no_wstool.bash from the workspace main folder (i.e. ~/catkin_ws). It will download all the packages in src/tofas_stiima. If some repositories are duplicated, remove the duplicated one.
* compile (install ros dependencies if needed)
* change robot ip address in src/tofas_stiima/tofas_human_awareness/tofas_universal_robot_control/config/ur10_hw.yaml

### USAGE ###

* you can connect with the robot using: roslaunch tofas_universal_robot_control ur10_control.launch        (keep the emergency stop close to you the first runs)
* in about 5 seconds, it will establish a connection and create a follow_joint_trajectory action (by default there is no namespace, let me know if you need to change) and start publish the joint_states
* launch MoveIt! launcher without launching the start ur_modern_controller.
* launch: roslaunch tofas_human_awareness tofas_human_awareness.launch to execute the wrapper between the human_detection, the off-line motion planner and the on-line reactive controller.
