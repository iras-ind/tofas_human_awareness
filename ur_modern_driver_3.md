[return to README.md](README.md)

Modifications required in the YOUR_PACKAGE_moveit_config/launch/trajectory_execution.launch.xml


Make sure to have this line to disable error on the trajectory duration (if a human enters in the scene, the robot will slow down)

```xml
<param name="move_group/trajectory_execution/execution_duration_monitoring" value="false"/>
```

[return to README.md](README.md)
